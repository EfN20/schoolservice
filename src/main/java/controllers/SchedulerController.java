package controllers;

import domain.models.Scheduler;
import filters.customAnnotations.OnlyDirector;
import services.SchedulerService;
import services.interfaces.ISchedulerServices;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("scheduler")
public class SchedulerController {
    private ISchedulerServices schedulerService = new SchedulerService();

    @OnlyDirector
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/add")
    public Response createNewScheduler(Scheduler scheduler) {
        try {
            schedulerService.addScheduler(scheduler);
        } catch (ServerErrorException ex) {
            return Response
                    .serverError().entity(ex.getMessage()).build();
        } catch (BadRequestException ex) {
            return Response
                    .status(Response.Status.BAD_REQUEST)
                    .entity(ex.getMessage()).build();
        }

        return Response.
                status(Response.Status.CREATED)
                .entity("Scheduler was created successfully!")
                .build();
    }

    @OnlyDirector
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/remove")
    public Response removeScheduler(Scheduler scheduler){
        try {
            schedulerService.removeScheduler(scheduler);
        } catch (ServerErrorException ex) {
            return Response
                    .serverError().entity(ex.getMessage()).build();
        } catch (BadRequestException ex) {
            return Response
                    .status(Response.Status.BAD_REQUEST)
                    .entity(ex.getMessage()).build();
        }

        return Response.
                status(Response.Status.CREATED)
                .entity("Scheduler was removed successfully!")
                .build();
    }

    @OnlyDirector
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/upgrade")
    public Response upgradeScheduler(Scheduler scheduler){
        try {
            schedulerService.updateScheduler(scheduler);
        } catch (ServerErrorException ex) {
            return Response
                    .serverError().entity(ex.getMessage()).build();
        } catch (BadRequestException ex) {
            return Response
                    .status(Response.Status.BAD_REQUEST)
                    .entity(ex.getMessage()).build();
        }

        return Response.
                status(Response.Status.CREATED)
                .entity("Scheduler was upgraded successfully!")
                .build();
    }


}
