package controllers;

import domain.models.Subject;
import filters.customAnnotations.OnlyDirector;
import services.SubjectService;
import services.interfaces.ISubjectService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("subject")
public class SubjectController {
    private ISubjectService subjectService = new SubjectService();

    @OnlyDirector
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/add")
    public Response addSubject(Subject subject){
        try {
            subjectService.addSubject(subject);
        } catch (ServerErrorException ex) {
            return Response
                    .serverError().entity(ex.getMessage()).build();
        } catch (BadRequestException ex) {
            return Response
                    .status(Response.Status.BAD_REQUEST)
                    .entity(ex.getMessage()).build();
        }

        return Response.
                status(Response.Status.CREATED)
                .entity("Subject was added successfully!")
                .build();
    }

    @OnlyDirector
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/remove")
    public Response removeSubject(Subject subject){
        try {
            subjectService.removeSubject(subject);
        } catch (ServerErrorException ex) {
            return Response
                    .serverError().entity(ex.getMessage()).build();
        } catch (BadRequestException ex) {
            return Response
                    .status(Response.Status.BAD_REQUEST)
                    .entity(ex.getMessage()).build();
        }

        return Response.
                status(Response.Status.CREATED)
                .entity("Subject was removed successfully!")
                .build();
    }

    @OnlyDirector
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/upgrade")
    public Response upgradeSubject(Subject subject){
        try {
            subjectService.updateSubject(subject);
        } catch (ServerErrorException ex) {
            return Response
                    .serverError().entity(ex.getMessage()).build();
        } catch (BadRequestException ex) {
            return Response
                    .status(Response.Status.BAD_REQUEST)
                    .entity(ex.getMessage()).build();
        }

        return Response.
                status(Response.Status.CREATED)
                .entity("Subject was upgraded successfully!")
                .build();
    }


}
