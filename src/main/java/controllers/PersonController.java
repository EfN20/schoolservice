package controllers;

import domain.models.Class;
import domain.models.Person;
import domain.models.Scheduler;
import filters.customAnnotations.OnlyTeacherAndDirector;
import filters.customAnnotations.JWTTokenNeeded;
import filters.customAnnotations.OnlyDirector;
import services.ClassService;
import services.PersonService;
import services.SchedulerService;
import services.interfaces.IClassService;
import services.interfaces.IPersonService;
import services.interfaces.ISchedulerServices;

import javax.ws.rs.*;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;


@Path("persons")
public class PersonController {
    private IPersonService personService = new PersonService();
    private ISchedulerServices schedulerService = new SchedulerService();
    private IClassService classService = new ClassService();

    @GET
    public String hello() {
        return "Hello world!";
    }

    @JWTTokenNeeded
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{id}")
    public Response getPersonById(@PathParam("id") long id) {
        Person person;
        try {
            person = personService.getPersonById(id);
        } catch (ServerErrorException ex) {
            return Response
                    .serverError().build();
        } catch (BadRequestException ex) {
            return Response
                    .status(Response.Status.ACCEPTED)
                    .entity("This person cannot be found").build();
        }

        if (person == null) {
            return Response
                    .status(Response.Status.NOT_FOUND)
                    .entity("Person does not exist!")
                    .build();
        } else {
            return Response
                    .status(Response.Status.OK)
                    .entity(person)
                    .build();
        }
    }

    @OnlyDirector
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/add")
    public Response createNewPerson(Person person) {
        try {
            personService.addPerson(person);
        } catch (ServerErrorException ex) {
            return Response
                    .serverError().entity(ex.getMessage()).build();
        } catch (BadRequestException ex) {
            return Response
                    .status(Response.Status.BAD_REQUEST)
                    .entity(ex.getMessage()).build();
        }

        return Response.
                status(Response.Status.CREATED)
                .entity("Person created successfully!")
                .build();
    }

    @JWTTokenNeeded
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/update")
    public Response updatePerson(Person person, @Context ContainerRequestContext requestContext) {
        if (!requestContext.getSecurityContext().isUserInRole("director") &&
                !requestContext.getSecurityContext().getUserPrincipal().getName().equals(person.getUsername())) {
            return Response
                    .status(Response.Status.FORBIDDEN)
                    .build();
        }

        try {
            personService.updatePerson(person);
        } catch (ServerErrorException ex) {
            return Response
                    .serverError().entity(ex.getMessage()).build();
        } catch (BadRequestException ex) {
            return Response
                    .status(Response.Status.BAD_REQUEST)
                    .entity(ex.getMessage()).build();
        }

        return Response.
                status(Response.Status.CREATED)
                .entity("Person updated successfully!")
                .build();
    }

    @OnlyDirector
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/{id}/delete")
    public Response removePerson(@PathParam("id") long id){
        try {
            personService.removePerson(personService.getPersonById(id));
        } catch (ServerErrorException ex) {
            return Response
                    .serverError().entity(ex.getMessage()).build();
        } catch (BadRequestException ex) {
            return Response
                    .status(Response.Status.BAD_REQUEST)
                    .entity(ex.getMessage()).build();
        }
        return Response.
                status(Response.Status.CREATED)
                .entity("Person deleted successfully!")
                .build();
    }

    @JWTTokenNeeded
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{subject_name}/teachers")
    public Response getSubjectsTeachers(@PathParam("subject_name") String subject_name){
        List<Person> teachers;
        try {
            teachers = personService.getSubjectsTeachers(subject_name);
        } catch (ServerErrorException ex){
            return Response.serverError().build();
        } catch (BadRequestException ex) {
            return Response
                    .status(Response.Status.ACCEPTED)
                    .entity("Teachers cannot be found").build();
        }
        if(teachers == null){
            return Response
                    .status(Response.Status.NOT_FOUND)
                    .entity("There are no teachers!")
                    .build();
        } else {
            return Response
                    .status(Response.Status.OK)
                    .entity(teachers)
                    .build();
        }
    }

    @JWTTokenNeeded
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{class_name}/students")
    public Response getClassmates(@PathParam("class_name") String class_name){
        List<Person> classmates;
        try {
            classmates = personService.getClassmates(class_name);
        } catch (ServerErrorException ex){
            return Response.serverError().build();
        } catch (BadRequestException ex) {
            return Response
                    .status(Response.Status.ACCEPTED)
                    .entity("Students cannot be found").build();
        }
        if(classmates == null){
            return Response
                    .status(Response.Status.NOT_FOUND)
                    .entity("There are no students in that class!")
                    .build();
        } else {
            return Response
                    .status(Response.Status.OK)
                    .entity(classmates)
                    .build();
        }
    }

    @OnlyTeacherAndDirector
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{surname}/teacher/scheduler")
    public Response getSchedulerForTeacher(@PathParam("surname") String surname){
        List<Scheduler> schedulers;
        try {
            schedulers = schedulerService.getSchedulerForTeacher(surname);
        } catch (ServerErrorException ex){
            return Response.serverError().build();
        } catch (BadRequestException ex) {
            return Response
                    .status(Response.Status.ACCEPTED)
                    .entity("This scheduler cannot be found").build();
        }
        if(schedulers == null){
            return Response
                    .status(Response.Status.NOT_FOUND)
                    .entity("Schedulers list is empty!")
                    .build();
        } else {
            return Response
                    .status(Response.Status.OK)
                    .entity(schedulers)
                    .build();
        }
    }

    @JWTTokenNeeded
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{class_name}/student/scheduler")
    public Response getSchedulerForStudent(@PathParam("class_name") String class_name){
        List<Scheduler> schedulers;
        try {
            schedulers = schedulerService.getSchedulerForStudent(class_name);
        } catch (ServerErrorException ex){
            return Response.serverError().build();
        } catch (BadRequestException ex) {
            return Response
                    .status(Response.Status.ACCEPTED)
                    .entity("This scheduler cannot be found").build();
        }
        if(schedulers == null){
            return Response
                    .status(Response.Status.NOT_FOUND)
                    .entity("Schedulers list is empty!")
                    .build();
        } else {
            return Response
                    .status(Response.Status.OK)
                    .entity(schedulers)
                    .build();
        }
    }

    @JWTTokenNeeded
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{id}/class")
    public Response getClassByStudentId(@PathParam("id") long id){
        Class whClass;
        try {
            whClass = classService.getClassByStudent(personService.getPersonById(id));
        } catch (ServerErrorException ex) {
            return Response
                    .serverError().build();
        } catch (BadRequestException ex) {
            return Response
                    .status(Response.Status.ACCEPTED)
                    .entity("This class cannot be found").build();
        }

        if (whClass == null) {
            return Response
                    .status(Response.Status.NOT_FOUND)
                    .entity("Class does not exist!")
                    .build();
        } else {
            return Response
                    .status(Response.Status.OK)
                    .entity(whClass)
                    .build();
        }
    }


}
