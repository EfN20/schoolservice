package controllers;

import domain.models.Class;
import filters.customAnnotations.OnlyTeacherAndDirector;
import filters.customAnnotations.OnlyDirector;
import services.ClassService;
import services.PersonService;
import services.interfaces.IClassService;
import services.interfaces.IPersonService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("class")
public class ClassController {
    private IPersonService personService = new PersonService();
    private IClassService classService = new ClassService();

    @OnlyDirector
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/add")
    public Response createNewClass(Class whClass) {
        try {
            classService.addClass(whClass);
        } catch (ServerErrorException ex) {
            return Response
                    .serverError().entity(ex.getMessage()).build();
        } catch (BadRequestException ex) {
            return Response
                    .status(Response.Status.BAD_REQUEST)
                    .entity(ex.getMessage()).build();
        }

        return Response.
                status(Response.Status.CREATED)
                .entity("Class was created successfully!")
                .build();
    }

    @OnlyDirector
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/remove")
    public Response removeClass(Class whClass){
        try {
            classService.removeClass(whClass);
        } catch (ServerErrorException ex) {
            return Response
                    .serverError().entity(ex.getMessage()).build();
        } catch (BadRequestException ex) {
            return Response
                    .status(Response.Status.BAD_REQUEST)
                    .entity(ex.getMessage()).build();
        }

        return Response.
                status(Response.Status.CREATED)
                .entity("Class was removed successfully!")
                .build();
    }

    @OnlyDirector
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/upgrade")
    public Response upgradeClass(Class whClass){
        try {
            classService.upgradeClass(whClass);
        } catch (ServerErrorException ex) {
            return Response
                    .serverError().entity(ex.getMessage()).build();
        } catch (BadRequestException ex) {
            return Response
                    .status(Response.Status.BAD_REQUEST)
                    .entity(ex.getMessage()).build();
        }

        return Response.
                status(Response.Status.CREATED)
                .entity("Class was upgraded successfully!")
                .build();
    }

    @OnlyTeacherAndDirector
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{class_id}")
    public Response getClassByClassId(@PathParam("class_id") long id){
        Class whClass;
        try {
            whClass = classService.getClassById(id) ;
        } catch (ServerErrorException ex) {
            return Response
                    .serverError().build();
        } catch (BadRequestException ex) {
            return Response
                    .status(Response.Status.ACCEPTED)
                    .entity("This class cannot be found").build();
        }

        if (whClass == null) {
            return Response
                    .status(Response.Status.NOT_FOUND)
                    .entity("Class does not exist!")
                    .build();
        } else {
            return Response
                    .status(Response.Status.OK)
                    .entity(whClass)
                    .build();
        }
    }

    @OnlyDirector
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/{class_id}/add/{id}")
    public Response addStudentToClass(@PathParam("id") long student_id, @PathParam("class_id") long class_id){
        try {
            classService.addStudentToClass(personService.getPersonById(student_id), classService.getClassById(class_id));
        } catch (ServerErrorException ex) {
            return Response
                    .serverError().entity(ex.getMessage()).build();
        } catch (BadRequestException ex) {
            return Response
                    .status(Response.Status.BAD_REQUEST)
                    .entity(ex.getMessage()).build();
        }

        return Response.
                status(Response.Status.CREATED)
                .entity("Student was added to class successfully!")
                .build();
    }

    @OnlyDirector
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/student/{id}/remove")
    public Response removeStudentFromClass(@PathParam("id") long student_id){
        try {
            classService.removeStudentFromClass(personService.getPersonById(student_id));
        } catch (ServerErrorException ex) {
            return Response
                    .serverError().entity(ex.getMessage()).build();
        } catch (BadRequestException ex) {
            return Response
                    .status(Response.Status.BAD_REQUEST)
                    .entity(ex.getMessage()).build();
        }

        return Response.
                status(Response.Status.CREATED)
                .entity("Student was removed from class successfully!")
                .build();
    }


}
