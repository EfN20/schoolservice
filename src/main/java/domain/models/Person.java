package domain.models;

public class Person {
    private long id;
    private String name;
    private String surname;
    private String username;
    private String password;
    private String role;

    public Person(){

    }

    public Person(long id, String name, String surname, String username, String password, String role){
        setId(id);
        setName(name);
        setSurname(surname);
        setUsername(username);
        setPassword(password);
        setRole(role);
    }

    public Person(long id, String name, String surname, String username, String role){
        setId(id);
        setName(name);
        setSurname(surname);
        setUsername(username);
        setRole(role);
    }

    public Person(String name, String surname, String username, String password, String role){
        setName(name);
        setSurname(surname);
        setUsername(username);
        setPassword(password);
        setRole(role);
    }

    public long getId (){return id;}

    public void setId (long id) {
        this.id = id;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname (String surname) {
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName (String name) {
        this.name = name;
    }

    public void setPassword (String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername (String username) {
        this.username = username;
    }

    public void setRole (String role) {
        this.role = role;
    }

    public String getRole() {
        return role;
    }

//    @Override
//    public String toString() {
//        return "Person{" +
//                "id=" + id +
//                ", name='" + name + '\'' +
//                ", surname='" + surname + '\'' +
//                ", username='" + username + '\'' +
//                ", password='" + password + '\'' +
//                ", role=" + role +
//                '}';
//    }
}
