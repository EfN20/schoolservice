package domain.models;

import java.util.List;

public class Class {
    private long id;
    private String class_name;
    private List<Person> classmates;

    public Class(){

    }

    public Class(long id, String class_name){
        setId(id);
        setClass_name(class_name);
    }

    public Class(long id, String class_name, List<Person> classmates){
        setId(id);
        setClass_name(class_name);
        setClassmates(classmates);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getClass_name() {
        return class_name;
    }

    public void setClass_name (String class_name) {
        this.class_name = class_name;
    }

    public void addStudentToClass (Person person){
        classmates.add(person);
    }

    public List<Person> getClassmates(){
        return classmates;
    }

    public void setClassmates(List<Person> classmates){
        this.classmates = classmates;
    }

}
