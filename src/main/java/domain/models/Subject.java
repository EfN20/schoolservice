package domain.models;

public class Subject {
    private long id;
    private String name;

    public Subject(){

    }

    public Subject(long id, String name){
        setId(id);
        setName(name);
    }

    public long getId(){
        return id;
    }

    public void setId(long id){
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
//b
}
