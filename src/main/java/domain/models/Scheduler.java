package domain.models;

public class Scheduler {
    private String person_name;
    private String person_surname;
    private String subject_name;
    private String weekDay;
    private String class_name;
    private String time;

    public Scheduler(){

    }

    public Scheduler(String person_name, String person_surname, String subject_name,
                     String weekDay, String class_name, String time){
        setPerson_name(person_name);
        setPerson_surname(person_surname);
        setSubject_name(subject_name);
        setWeekDay(weekDay);
        setClass_name(class_name);
        setTime(time);
    }

    public String getPerson_name() {
        return person_name;
    }

    public void setPerson_name(String person_name) {
        this.person_name = person_name;
    }

    public String getPerson_surname() {
        return person_surname;
    }

    public void setPerson_surname(String person_surname) {
        this.person_surname = person_surname;
    }

    public String getWeekDay() {
        return weekDay;
    }

    public void setWeekDay(String weekDay) {
        this.weekDay = weekDay;
    }

    public String getClass_name() {
        return class_name;
    }

    public void setClass_name(String class_name) {
        this.class_name = class_name;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getSubject_name() {
        return subject_name;
    }

    public void setSubject_name(String subject_name) {
        this.subject_name = subject_name;
    }

}
