package services;

import domain.models.Subject;
import repositories.SubjectRepository;
import repositories.interfaces.ISubjectRepository;
import services.interfaces.ISubjectService;

public class SubjectService implements ISubjectService {
    private ISubjectRepository subjectRepo = new SubjectRepository();


    @Override
    public void addSubject(Subject subject) {
        subjectRepo.add(subject);
    }

    @Override
    public void removeSubject(Subject subject) {
        subjectRepo.remove(subject);
    }

    @Override
    public void updateSubject(Subject subject) {
        subjectRepo.update(subject);
    }

    @Override
    public Subject getSubjectById(long id) {
        return subjectRepo.getSubjectById(id);
    }

    @Override
    public Subject getSubjectByName(String name) {
        return subjectRepo.getSubjectByName(name);
    }

}
