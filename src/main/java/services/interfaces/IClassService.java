package services.interfaces;

import domain.models.Class;
import domain.models.Person;

public interface IClassService {
    Class getClassById(long id);

    Class getClassByName(String name);

    Class getClassByStudent(Person person);

    void addStudentToClass(Person person, Class whClass);

    void removeStudentFromClass(Person person);

    void addClass(Class whClass);

    void removeClass(Class whClass);

    void upgradeClass(Class whClass);

}
