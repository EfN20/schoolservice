package services.interfaces;

import domain.models.Subject;

public interface ISubjectService {
    void addSubject(Subject subject);

    void removeSubject(Subject subject);

    void updateSubject(Subject subject);

    Subject getSubjectById(long id);

    Subject getSubjectByName(String name);

}
