package services.interfaces;

import domain.AccessToken;
import domain.PersonLoginData;
import domain.models.Person;

public interface IAuthorizationService {
    AccessToken authenticatePerson(PersonLoginData data) throws Exception;

    Person getPersonByUsername(String issuer);

}
