package services.interfaces;

import domain.models.Person;

import java.util.List;

public interface IPersonService {
    Person getPersonById(long id);

    Person getPersonBySurname(String surname);

    List<Person> getSubjectsTeachers(String subject_name);

    List<Person> getClassmates(String class_name);

    void addPerson(Person person);

    void updatePerson(Person person);

    void removePerson(Person person);

}
