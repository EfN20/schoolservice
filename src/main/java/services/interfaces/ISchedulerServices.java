package services.interfaces;

import domain.models.Scheduler;

import java.util.List;

public interface ISchedulerServices {
    void addScheduler(Scheduler scheduler);

    void removeScheduler(Scheduler scheduler);

    void updateScheduler(Scheduler scheduler);

    List<Scheduler> getSchedulerForTeacher(String surname);

    List<Scheduler> getSchedulerForStudent(String class_name);

}
