package services;

import domain.AccessToken;
import domain.PersonLoginData;
import domain.models.Person;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import repositories.PersonRepository;
import repositories.interfaces.IPersonRepository;
import services.interfaces.IAuthorizationService;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.Random;


public class AuthorizationService implements IAuthorizationService {

    private final IPersonRepository personRepository = new PersonRepository();

    public AccessToken authenticatePerson(PersonLoginData data) throws Exception {
        Person authenticatedUser = signIn(data);
        return new AccessToken(issueToken(authenticatedUser));
    }

    @Override
    public Person getPersonByUsername(String issuer) {
        return personRepository.getPersonByUsername(issuer);
    }

    private Person signIn(PersonLoginData data) throws Exception {
        Person person = personRepository.findPersonByLogin(data);
        if (person == null) {
            throw new Exception("Authentication failed!");
        }
        return person;
    }

    private String issueToken(Person person) {
        Instant now = Instant.now();
        String secretWord = "TheStrongestSecretKeyICanThinkOf";
        return Jwts.builder()
                .setIssuer(person.getUsername())
                .setIssuedAt(Date.from(now))
                .claim("1d20", new Random().nextInt(20) + 1)
                .setExpiration(Date.from(now.plus(10, ChronoUnit.MINUTES)))
                .signWith(Keys.hmacShaKeyFor(secretWord.getBytes()))
                .compact();

    }

}
