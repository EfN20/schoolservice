package services;

import domain.models.Person;
import repositories.PersonRepository;
import repositories.interfaces.IPersonRepository;
import services.interfaces.IPersonService;

import java.util.List;

public class PersonService implements IPersonService {
    private IPersonRepository personRepo = new PersonRepository();

    public Person getPersonById(long id){
        return personRepo.getPersonById(id);
    }

    @Override
    public Person getPersonBySurname(String surname) {
        return personRepo.getPersonBySurname(surname);
    }

    @Override
    public List<Person> getSubjectsTeachers(String subject_name) {
        return personRepo.getSubjectsTeachers(subject_name);
    }

    @Override
    public List<Person> getClassmates(String class_name) {
        return personRepo.getClassmates(class_name);
    }

    public void addPerson(Person person) {
        personRepo.add(person);
    }

    @Override
    public void updatePerson(Person person) {
        personRepo.update(person);
    }

    @Override
    public void removePerson(Person person) {
        personRepo.remove(person);
    }
    //b
}
