package services;

import domain.models.Scheduler;
import repositories.SchedulerRepository;
import repositories.interfaces.ISchedulerRepository;
import services.interfaces.ISchedulerServices;

import java.util.List;

public class SchedulerService implements ISchedulerServices {
    private ISchedulerRepository schedulerRepo = new SchedulerRepository();

    @Override
    public void addScheduler(Scheduler scheduler) {
        schedulerRepo.add(scheduler);
    }

    @Override
    public void removeScheduler(Scheduler scheduler) {
        schedulerRepo.remove(scheduler);
    }

    @Override
    public void updateScheduler(Scheduler scheduler) {
        schedulerRepo.update(scheduler);
    }

    @Override
    public List<Scheduler> getSchedulerForTeacher(String surname) {
        return schedulerRepo.getSchedulerForTeacher(surname);
    }

    @Override
    public List<Scheduler> getSchedulerForStudent(String class_name) {
        return schedulerRepo.getSchedulerForStudent(class_name);
    }

}
