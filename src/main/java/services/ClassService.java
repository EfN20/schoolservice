package services;

import domain.models.Class;
import domain.models.Person;
import repositories.ClassRepository;
import repositories.interfaces.IClassRepository;
import services.interfaces.IClassService;

public class ClassService implements IClassService {
    private IClassRepository classRepo = new ClassRepository();

    @Override
    public void addClass(Class whClass) {
        classRepo.add(whClass);
    }

    @Override
    public void removeClass(Class whClass) {
        classRepo.remove(whClass);
    }

    @Override
    public void upgradeClass(Class whClass) {
        classRepo.update(whClass);
    }

    @Override
    public Class getClassById(long id) {
        return classRepo.getClassById(id);
    }

    @Override
    public Class getClassByName(String name) {
        return classRepo.getClassByName(name);
    }

    @Override
    public Class getClassByStudent(Person person) {
        return classRepo.getClassByStudent(person);
    }

    @Override
    public void addStudentToClass(Person person, Class whClass) {
        classRepo.addStudentToClass(person, whClass);
    }

    @Override
    public void removeStudentFromClass(Person person) {
        classRepo.removeStudentFromClass(person);
    }


}
