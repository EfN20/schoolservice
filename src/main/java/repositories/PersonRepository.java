package repositories;

import domain.PersonLoginData;
import domain.models.Person;
import repositories.interfaces.IDBRepository;
import repositories.interfaces.IPersonRepository;

import javax.ws.rs.BadRequestException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class PersonRepository implements IPersonRepository {
    private IDBRepository dbrepo = new PostgresRepository();

    @Override
    public void add(Person entity) {
        try {
            String sql = "INSERT INTO persons(id, name, surname, username, password, role) " +
                    "VALUES(?, ?, ?, ?, ?, ?)";
            PreparedStatement stmt = dbrepo.getConnection().prepareStatement(sql);
            stmt.setLong(1, entity.getId());
            stmt.setString(2, entity.getName());
            stmt.setString(3, entity.getSurname());
            stmt.setString(4, entity.getUsername());
            stmt.setString(5, entity.getPassword());
            stmt.setString(6, entity.getRole());

            stmt.execute();
        } catch (SQLException ex) {
            throw new BadRequestException("Cannot run SQL statement: " + ex.getMessage());
        }
    }

    @Override
    public void update(Person entity) {
        String sql = "UPDATE persons " +
                "SET ";
        if (entity.getName() != null) {
            sql += "name=?, ";
        }
        if (entity.getSurname() != null) {
            sql += "surname=?, ";
        }
        if (entity.getPassword() != null) {
            sql += "password=?, ";
        }
        if (entity.getRole() != null) {
            sql += "role=?, ";
        }

        sql = sql.substring(0, sql.length() - 2);

        sql += " WHERE username = ?";

        try {
            int i = 1;
            PreparedStatement stmt = dbrepo.getConnection().prepareStatement(sql);
            if (entity.getName() != null) {
                stmt.setString(i++, entity.getName());
            }
            if (entity.getSurname() != null) {
                stmt.setString(i++, entity.getSurname());
            }
            if (entity.getPassword() != null) {
                stmt.setString(i++, entity.getPassword());
            }
            if (entity.getRole() != null) {
                stmt.setString(i++, entity.getRole());
            }
            stmt.setString(i++, entity.getUsername());

            stmt.execute();
        } catch (SQLException ex) {
            throw new BadRequestException("Cannot run SQL statement: " + ex.getMessage());
        }
    }

    @Override
    public void remove(Person entity) {
        String sql = "Delete from persons where id = " + entity.getId();
        try {
            PreparedStatement stmt = dbrepo.getConnection().prepareStatement(sql);
            stmt.execute();
        } catch (SQLException ex){
            throw new BadRequestException("Cannot run SQL statement: " + ex.getMessage());
        }
    }

    @Override
    public List<Person> queryTwo(String sql) {
        try {
            Statement stmt = dbrepo.getConnection().createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            LinkedList<Person> persons = new LinkedList<>();
            while (rs.next()) {
                Person person = new Person(
                        rs.getLong("id"),
                        rs.getString("name"),
                        rs.getString("surname"),
                        rs.getString("username"),
                        rs.getString("password"),
                        rs.getString("role")
                );
                persons.add(person);
            }
            return persons;
        } catch (SQLException ex) {
            throw new BadRequestException("Cannot run SQL statement: " + ex.getSQLState());
        }
    }

    @Override
    public Person queryOne(String sql) {
        try {
            Statement stmt = dbrepo.getConnection().createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                return new Person(
                        rs.getLong("id"),
                        rs.getString("name"),
                        rs.getString("surname"),
                        rs.getString("username"),
                        rs.getString("password"),
                        rs.getString("role")
                );
            }
        } catch (SQLException ex) {
            throw new BadRequestException("Cannot run SQL statement: " + ex.getMessage());
        }
        return null;
    }


    @Override
    public List<Person> queryThree(String sql) {
        return null;
    }

    public Person getPersonById(long id) {
        String sql = "SELECT * FROM persons WHERE id = " + id + " LIMIT 1";
        return queryOne(sql);
    }

    public Person getPersonByUsername(String username) {
        String sql = "SELECT * FROM persons WHERE username = '" + username + "' LIMIT 1";
        return queryOne(sql);
    }

    @Override
    public Person getPersonBySurname(String surname) {
        String sql = "SELECT * FROM persons WHERE surname = '" + surname + "' LIMIT 1";
        return queryOne(sql);
    }

    public Person findPersonByLogin(PersonLoginData data) {
        try {
            String sql = "SELECT * FROM persons WHERE username = ? AND password = ?";
            PreparedStatement stmt = dbrepo.getConnection().prepareStatement(sql);
            stmt.setString(1, data.getUsername());
            stmt.setString(2, data.getPassword());
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                return new Person(
                        rs.getLong("id"),
                        rs.getString("name"),
                        rs.getString("surname"),
                        rs.getString("username"),
                        rs.getString("password"),
                        rs.getString("role")
                );
            }
        } catch (SQLException ex) {
            throw new BadRequestException("Cannot run SQL statement: " + ex.getMessage());
        }
        return null;
    }

    @Override
    public List<Person> getSubjectsTeachers(String subject_name) {
        String sql = "select DISTINCT persons.id, persons.name, persons.surname, persons.username, persons.password, persons.role " +
                "from scheduler " +
                "inner join persons on persons.id = scheduler.teacher_id " +
                "inner join subjects on subjects.id = scheduler.subj_id " +
                "where subjects.name = '" + subject_name + "'";
        return queryTwo(sql);
    }

    @Override
    public List<Person> getClassmates(String class_name) {
        String sql = "Select Distinct persons.id, persons.name, persons.surname, persons.username, persons.password, persons.role " +
                "from class_members " +
                "inner join persons on persons.id = class_members.stud_id " +
                "inner join classes on classes.id = class_members.class_id " +
                "where classes.name = '" + class_name + "'";
        return queryTwo(sql);
    }

}
