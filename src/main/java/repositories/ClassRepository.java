package repositories;

import domain.models.Class;
import domain.models.Person;
import domain.models.Scheduler;
import repositories.interfaces.IClassRepository;
import repositories.interfaces.IDBRepository;


import javax.ws.rs.BadRequestException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class ClassRepository implements IClassRepository {
    private IDBRepository dbrepo = new PostgresRepository();

    @Override
    public void add(Class entity) {
        try {
            String sql = "INSERT INTO classes(id, name) " +
                    "VALUES(?, ?)";
            PreparedStatement stmt = dbrepo.getConnection().prepareStatement(sql);
            stmt.setLong(1, entity.getId());
            stmt.setString(2, entity.getClass_name());
            stmt.execute();
        } catch (SQLException ex) {
            throw new BadRequestException("Cannot run SQL statement: " + ex.getMessage());
        }
    }

    @Override
    public void update(Class entity) {
        String sql = "upgrade classes set name = ? where id = ?";
        try {
            PreparedStatement stmt = dbrepo.getConnection().prepareStatement(sql);
            stmt.setString(1, entity.getClass_name());
            stmt.setLong(2, entity.getId());
            stmt.execute();
        } catch (SQLException ex){
            throw new BadRequestException("Cannot run SQL statement: " + ex.getMessage());
        }
    }

    @Override
    public void remove(Class entity) {
        String sql = "Delete from classes where id = " + entity.getId();
        try {
            PreparedStatement stmt = dbrepo.getConnection().prepareStatement(sql);
            stmt.execute();
        } catch (SQLException ex){
            throw new BadRequestException("Cannot run SQL statement: " + ex.getMessage());
        }
    }

    @Override
    public Class queryOne(String sql) {
        try {
            Statement stmt = dbrepo.getConnection().createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                return new Class(
                        rs.getLong("id"),
                        rs.getString("name")
                );
            }
        } catch (SQLException ex) {
            throw new BadRequestException("Cannot run SQL statement: " + ex.getMessage());
        }
        return null;
    }

    @Override
    public List<Class> queryTwo(String sql) {
        return null;
    }

    @Override
    public List<Class> queryThree(String sql) {
        return null;
    }

    @Override
    public Class getClassById(long id) {
        String sql = "select * from classes where id = " + id;
        return queryOne(sql);
    }

    @Override
    public Class getClassByName(String name) {
        String sql = "SELECT * FROM classes WHERE name = '" + name + "'";
        return queryOne(sql);
    }

    @Override
    public Class getClassByStudent(Person person) {
        String sql = "select classes.id, classes.name from class_members " +
                "inner join persons on persons.id = class_members.stud_id " +
                "inner join classes on classes.id = class_members.class_id " +
                "where persons.id = " + person.getId();
        return queryOne(sql);
    }

    @Override
    public void addStudentToClass(Person person, Class whClass) {
        try {
            String sql = "insert into class_members(class_id, stud_id) " +
                    "values " +
                    "(?, ?)";
            PreparedStatement stmt = dbrepo.getConnection().prepareStatement(sql);
            stmt.setLong(1, whClass.getId());
            stmt.setLong(2, person.getId());
            stmt.execute();

        } catch (SQLException ex){
            throw new BadRequestException("Cannot run SQL statement: " + ex.getMessage());
        }
    }

    @Override
    public void removeStudentFromClass(Person person) {
        try {
            String sql = "Delete from class_members where stud_id = ?";
            PreparedStatement stmt = dbrepo.getConnection().prepareStatement(sql);
            stmt.setLong(1, person.getId());
            stmt.execute();

        } catch (SQLException ex){
            throw new BadRequestException("Cannot run SQL statement: " + ex.getMessage());
        }
    }

}
