package repositories;

import domain.models.Scheduler;
import repositories.interfaces.*;

import javax.ws.rs.BadRequestException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class SchedulerRepository implements ISchedulerRepository {
    private IDBRepository dbrepo = new PostgresRepository();
    private Statement stmt;
    private List<Scheduler> schedulers = new ArrayList<Scheduler>();
    private IClassRepository classRepo = new ClassRepository();
    private IPersonRepository personRepo = new PersonRepository();
    private ISubjectRepository subjectRepo = new SubjectRepository();

    @Override
    public List<Scheduler> getSchedulerForTeacher(String surname) {
        String sql = "Select persons.id, persons.name as person_name, persons.surname, weekday, subjects.name as subject_name, classes.name as class_name, time from scheduler " +
                    "inner join persons on scheduler.teacher_id = persons.id " +
                    "inner join classes on scheduler.class_id = classes.id " +
                    "inner join subjects on scheduler.subj_id = subjects.id " +
                    "where persons.id = " + personRepo.getPersonBySurname(surname).getId() + " and persons.role = 'Teacher' " +
                    "order by persons.id;";
        return queryTwo(sql);
    }

    @Override
    public List<Scheduler> getSchedulerForStudent(String class_name) {
        String sql = "Select time, subjects.name as subject_name, classes.name as class_name, weekday, persons.name as person_name, persons.surname from scheduler " +
                "inner join persons on scheduler.teacher_id = persons.id " +
                "inner join classes on scheduler.class_id = classes.id " +
                "inner join subjects on scheduler.subj_id = subjects.id " +
                "where classes.name = '" + class_name +
                "' ORDER BY " +
                "CASE " +
                "WHEN weekday = 'Monday' THEN 1 " +
                "WHEN weekday = 'Tuesday' THEN 2 " +
                "WHEN weekday = 'Wednesday' THEN 3 " +
                "WHEN weekday = 'Thursday' THEN 4 " +
                "WHEN weekday = 'Friday' THEN 5 " +
                "END ASC";
        return queryThree(sql);
    }

    @Override
    public void add(Scheduler entity) {
        try {
            String sql = "insert into scheduler(class_id, weekday, subj_id, time, teacher_id) " +
                    "values (?, ?, ?, ?, ?)";
            PreparedStatement stmt = dbrepo.getConnection().prepareStatement(sql);
            stmt.setLong(1, classRepo.getClassByName(entity.getClass_name()).getId() );
            stmt.setString(2, entity.getWeekDay());
            stmt.setLong(3, subjectRepo.getSubjectByName(entity.getSubject_name()).getId());
            stmt.setString(4, entity.getTime());
            stmt.setLong(5, personRepo.getPersonBySurname(entity.getPerson_surname()).getId());

        } catch (SQLException ex) {
            throw new BadRequestException("Cannot run SQL statement: " + ex.getMessage());
        }
    }

    @Override
    public void update(Scheduler entity) {
        String sql = "UPDATE scheduler " +
                "SET ";
        if (entity.getClass_name() != null) {
            sql += "class_id = ?, ";
        }
        if (entity.getWeekDay() != null) {
            sql += "weekday = ?, ";
        }
        if (entity.getSubject_name() != null) {
            sql += "subject_id = ?, ";
        }
        if (entity.getPerson_surname() != null) {
            sql += "teached_id = ?, ";
        }

        sql = sql.substring(0, sql.length() - 2);

        sql += " WHERE time = ?";

        try {
            int i = 1;
            PreparedStatement stmt = dbrepo.getConnection().prepareStatement(sql);
            if (entity.getClass_name() != null) {
                stmt.setLong(i++, classRepo.getClassByName(entity.getClass_name()).getId());
            }
            if (entity.getWeekDay() != null) {
                stmt.setString(i++, entity.getWeekDay());
            }
            if (entity.getSubject_name() != null) {
                stmt.setLong(i++, subjectRepo.getSubjectByName(entity.getSubject_name()).getId());
            }
            if (entity.getPerson_surname() != null ) {
                stmt.setLong(i++, personRepo.getPersonBySurname(entity.getPerson_surname()).getId());
            }
            stmt.setString(i++, entity.getTime());

            stmt.execute();
        } catch (SQLException ex) {
            throw new BadRequestException("Cannot run SQL statement: " + ex.getMessage());
        }
    }

    @Override
    public void remove(Scheduler entity) {
        String sql = "delete from scheduler " +
                "where ";
        if (entity.getClass_name() != null) {
            sql += "class_id = ?, ";
        }
        if (entity.getWeekDay() != null) {
            sql += "weekday = ?, ";
        }
        if (entity.getSubject_name() != null) {
            sql += "subject_id = ?, ";
        }
        if (entity.getPerson_surname() != null) {
            sql += "teached_id = ?, ";
        }
        if (entity.getTime() != null) {
            sql += " time = ? ;";
        }

        sql = sql.substring(0, sql.length() - 2);

        try {
            int i = 1;
            PreparedStatement stmt = dbrepo.getConnection().prepareStatement(sql);
            if (entity.getClass_name() != null) {
                stmt.setLong(i++, classRepo.getClassByName(entity.getClass_name()).getId());
            }
            if (entity.getWeekDay() != null) {
                stmt.setString(i++, entity.getWeekDay());
            }
            if (entity.getSubject_name() != null) {
                stmt.setLong(i++, subjectRepo.getSubjectByName(entity.getSubject_name()).getId());
            }
            if (entity.getPerson_surname() != null ) {
                stmt.setLong(i++, personRepo.getPersonBySurname(entity.getPerson_surname()).getId());
            }
            if (entity.getTime() != null ) {
                stmt.setString(i++, entity.getTime());
            }

            stmt.execute();
        } catch (SQLException ex) {
            throw new BadRequestException("Cannot run SQL statement: " + ex.getMessage());
        }
    }

    @Override
    public Scheduler queryOne(String sql) {
        return null;
    }

    @Override
    public List<Scheduler> queryTwo(String sql) {
        try {
            stmt = dbrepo.getConnection().createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while(rs.next()){
                 Scheduler scheduler = new Scheduler(
                    rs.getString("person_name"),
                    rs.getString("surname"),
                    rs.getString("weekday"),
                    rs.getString("subject_name"),
                    rs.getString("class_name"),
                    rs.getString("time")
                );
                 schedulers.add(scheduler);
            }
            return schedulers;
        } catch (SQLException ex){
            throw new BadRequestException();
        }
    }

    @Override
    public List<Scheduler> queryThree(String sql) {
        try {
            stmt = dbrepo.getConnection().createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while(rs.next()){
                Scheduler scheduler = new Scheduler(
                        rs.getString("person_name"),
                        rs.getString("surname"),
                        rs.getString("weekday"),
                        rs.getString("subject_name"),
                        rs.getString("class_name"),
                        rs.getString("time")
                );
                schedulers.add(scheduler);
            }
            return schedulers;
        } catch (SQLException ex){
            throw new BadRequestException();
        }
    }

}