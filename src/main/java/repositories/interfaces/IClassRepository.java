package repositories.interfaces;

import domain.models.Class;
import domain.models.Person;

public interface IClassRepository extends IEntityRepository<Class> {
    Class getClassById(long id);

    Class getClassByName(String name);

    Class getClassByStudent(Person person);

    void addStudentToClass(Person person, Class whClass);

    void removeStudentFromClass(Person person);

}
