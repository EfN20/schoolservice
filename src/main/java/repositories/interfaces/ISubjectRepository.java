package repositories.interfaces;

import domain.models.Subject;

public interface ISubjectRepository extends IEntityRepository<Subject>{
    Subject getSubjectById(long id);

    Subject getSubjectByName(String name);

}
