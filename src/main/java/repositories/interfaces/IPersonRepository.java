package repositories.interfaces;

import domain.PersonLoginData;
import domain.models.Person;

import java.util.List;

public interface IPersonRepository extends IEntityRepository<Person> {
    Person getPersonById(long id);

    Person getPersonByUsername(String username);

    Person getPersonBySurname(String surname);

    Person findPersonByLogin(PersonLoginData data);

    List<Person> getSubjectsTeachers(String subject_name);

    List<Person> getClassmates(String class_name);

}
