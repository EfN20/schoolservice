package repositories.interfaces;

import domain.models.Scheduler;

import java.util.List;

public interface ISchedulerRepository extends IEntityRepository<Scheduler> {
    List<Scheduler> getSchedulerForTeacher(String surname);

    List<Scheduler> getSchedulerForStudent(String class_name);

}
