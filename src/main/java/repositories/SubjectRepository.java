package repositories;

import domain.models.Subject;
import repositories.interfaces.IDBRepository;
import repositories.interfaces.ISubjectRepository;

import javax.ws.rs.BadRequestException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class SubjectRepository implements ISubjectRepository {
    private IDBRepository dbRepo = new PostgresRepository();

    @Override
    public void add(Subject entity) {
        try {
            String sql = "INSERT INTO subjects(id, name) " +
                    "VALUES(?, ?)";
            PreparedStatement stmt = dbRepo.getConnection().prepareStatement(sql);
            stmt.setLong(1, entity.getId());
            stmt.setString(2, entity.getName());
            stmt.execute();
        } catch (SQLException ex) {
            throw new BadRequestException("Cannot run SQL statement: " + ex.getMessage());
        }
    }

    @Override
    public void update(Subject entity) {
        String sql = "upgrade subjects set name = ? where id = ?";
        try {
            PreparedStatement stmt = dbRepo.getConnection().prepareStatement(sql);
            stmt.setString(1, entity.getName());
            stmt.setLong(2, entity.getId());
            stmt.execute();
        } catch (SQLException ex){
            throw new BadRequestException("Cannot run SQL statement: " + ex.getMessage());
        }
    }

    @Override
    public void remove(Subject entity) {
        String sql = "Delete from subjects where id = " + entity.getId();
        try {
            PreparedStatement stmt = dbRepo.getConnection().prepareStatement(sql);
            stmt.execute();
        } catch (SQLException ex){
            throw new BadRequestException("Cannot run SQL statement: " + ex.getMessage());
        }
    }

    @Override
    public Subject queryOne(String sql) {
        try {
            Statement stmt = dbRepo.getConnection().createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                return new Subject(
                        rs.getLong("id"),
                        rs.getString("name")
                );
            }
        } catch (SQLException ex) {
            throw new BadRequestException("Cannot run SQL statement: " + ex.getMessage());
        }
        return null;
    }

    @Override
    public List<Subject> queryTwo(String sql) {
        return null;
    }

    @Override
    public List<Subject> queryThree(String sql) {
        return null;
    }

    @Override
    public Subject getSubjectById(long id) {
        String sql = "select * from subjects where id = " + id;
        return queryOne(sql);
    }

    @Override
    public Subject getSubjectByName(String name) {
        String sql = "SELECT * FROM subjects WHERE name = '" + name + "'";
        return queryOne(sql);
    }

}
